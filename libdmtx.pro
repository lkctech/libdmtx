#-------------------------------------------------
#
# Project created by QtCreator 2014-01-08T10:54:41
#
#-------------------------------------------------

QT       -= gui

TARGET = libdmtx
TEMPLATE = lib

DEFINES += LIBDMTX_LIBRARY

SOURCES += \
    dmtx.c

HEADERS +=\
        libdmtx_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
